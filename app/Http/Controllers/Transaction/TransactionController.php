<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Transaction;

class TransactionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('scope:read-general')->only('show');
        $this->middleware('can:view,transaction')->only('show');
    }

    public function index()
    {
        $this->allowedAdminAction();

        $data = Transaction::all();

        return $this->showAll($data);
    }

    public function show(Transaction $transaction)
    {
        return $this->showOne($transaction);
    }
}
