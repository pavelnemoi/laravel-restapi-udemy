<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Product;
use App\User;
use App\Transaction;
use Illuminate\Support\Facades\DB;
use App\Transformers\TransactionTransformer;

class ProductBuyerTransactionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->middleware('transform.input:' . TransactionTransformer::class)
            ->only(['store']);
        
        $this
            ->middleware('scope:purchase-product')
            ->only(['store']);

        $this->middleware('can:purchase,buyer')->only('store');            
    }


    public function store(Request $request, Product $product, User $buyer)
    {
        $rules = [
            'quantity' => 'required|int|min:1'
        ];
        
        $this->validate($request, $rules);
        
        if ($product->seller_id === $buyer->id) {
            return $this->errorResponse(
                'The buyer must be different from the seller',
                409
            );
        }

        if (! $buyer->isVerified()) {
            return $this->errorResponse(
                'The buyer must be verified user',
                409
            );
        }

        if (! $product->seller->isVerified()) {
            return $this->errorResponse(
                'The seller must be verified user',
                409
            );
        }

        if (! $product->isAvailable()) {
            return $this->errorResponse(
                'The product is not available',
                409
            );
        }

        if ($product->quantity < $request->quantity) {
            return $this->errorResponse(
                'The product\'s quantity is less than requested',
                409
            );
        }

        return DB::transaction(function() use($request, $product, $buyer) {
            $product->quantity -= $request->quantity;
            $product->save();

            $transaction = Transaction::create([
                'quantity' => $request->quantity,
                'buyer_id' => $buyer->id,
                'product_id' => $product->id
            ]);

            return $this->showOne($transaction, 201);
        });
    }
}
