<?php 

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

trait ApiResponse 
{
	private function successResponse(array $data, int $status)
	{
		return response()->json($data, $status);
	}

	public function errorResponse($msg, int $status)
	{
		return response()->json(['error' => $msg, 'status' => $status], $status);
	}

	protected function showAll(Collection $collection, int $status = 200)
	{
		if ($collection->isEmpty()) {
			return $this->successResponse(['data' => []], $status);	
		}

		$transformer = $collection->first()->transformer;
		$collection = $this->filterData($collection, $transformer);		
		$collection = $this->sortData($collection, $transformer);
		$collection = $this->paginate($collection);
		$collection = $this->transformData($collection, $transformer);
		// $collection = $this->cacheResponse($collection);

		return $this->successResponse($collection, $status);
	}

	protected function showOne(Model $instance, int $status = 200)
	{
		$data = $this->transformData($instance, $instance->transformer);

		return $this->successResponse($data, $status);
	}

	protected function showMessage(string $msg)
	{
		return $this->successResponse(['data' => $msg], 200);
	}

	protected function transformData($data, $transformer)
	{
		$data = fractal($data, new $transformer);

		return $data->toArray();
	}

	protected function filterData(Collection $collection, $transformer)
	{
		foreach (request()->query() as $query => $value)
		{
			$attr = $transformer::originalAttribute($query);

			if (isset($attr, $value)) {
				$collection = $collection->where($attr, $value);
			}
		}
		
		return $collection;
	}

	protected function sortData(Collection $collection, $transformer)
	{
		if (request()->has('sort_by')) {
			$attr = $transformer::originalAttribute(request()->sort_by);
			$collection = $collection->sortBy->{$attr};
		}

		return $collection;
	}

	protected function paginate(Collection $collection)
	{
		$rules = [
			'per_page' => 'int|min:2|max:50'
		];
		
		Validator::validate(request()->all(), $rules);

		$page = LengthAwarePaginator::resolveCurrentPage();

		$perPage = 15;

		if (request()->has('per_page')) {
			$perPage = (int)request()->per_page;
		}

		$results = $collection->slice(($page - 1) * $perPage, $perPage)->values();

		$paginated = new LengthAwarePaginator(
			$results, 
			$collection->count(), 
			$perPage,
			$page,
			[
				'path' => LengthAwarePaginator::resolveCurrentPath()
			]
		);

		$paginated->appends(request()->all());

		return $paginated;
	}

	protected function cacheResponse($data)
	{
		$url = request()->url();
		$query = request()->query();
		ksort($query);
		$query = http_build_query($query);
		$fullUrl = "{$url}?{$query}";

		return Cache::remember($fullUrl, 30 / 60, function() use($data) {
			return $data;
		});
	}
}