<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\AdminActions;

class UserPolicy
{
    use HandlesAuthorization;
    use AdminActions;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $authUser
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $authUser, User $model)
    {
        return $authUser->id === $model->id;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $authUser
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $authUser, User $model)
    {
        return $authUser->id === $model->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $authUser
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $authUser, User $model)
    {
        return $authUser->id === $model->id && $authUser->token()
            ->client
            ->personal_access_client;
    }
}
