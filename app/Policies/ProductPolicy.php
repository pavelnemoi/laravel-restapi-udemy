<?php

namespace App\Policies;

use App\User;
use App\Product;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\AdminActions;

class ProductPolicy
{
    use HandlesAuthorization;
    use AdminActions;

    public function addCategory(User $user, Product $product)
    {
        return $user->id === $product->seller->id;
    }

    public function deleteCategory(User $user, Product $product)
    {
        return $user->id === $product->seller->id;
    }
}
