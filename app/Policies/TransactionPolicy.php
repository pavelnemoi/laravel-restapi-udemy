<?php

namespace App\Policies;

use App\User;
use App\Transaction;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\AdminActions;

class TransactionPolicy
{
    use HandlesAuthorization;
    use AdminActions;

    public function view(User $user, Transaction $transaction)
    {
        return $user->id === $transaction->buyer->id || $user->id === $transaction->product
            ->seller->id;
    }
}
