@component('mail::message')
# Hello {{ $user->name }}

Please click the link:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Verify email
@endcomponent

Thanks,<br>
@endcomponent
