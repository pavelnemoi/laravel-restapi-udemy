@component('mail::message')
# Hello {{ $user->name }}

Please confirm address below:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Verify email
@endcomponent

Thanks,<br>
@endcomponent
