<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Product;
use App\Category;
use App\Transaction;

class DatabaseSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        DB::table('category_product')->truncate();
        

        // clear events
        User::flushEventListeners();
        Category::flushEventListeners();
        Product::flushEventListeners();
        Transaction::flushEventListeners();    

        $usersCount = 100;
        $categoriesCount = 100;
        $productsCount = 100;
        $transactionsCount = 100;

        factory(User::class, $usersCount)->create();
        factory(Category::class, $categoriesCount)->create();
        factory(Product::class, $productsCount)->create()->each(
        	function(Product $product) {
        		$categoryIds = Category::all()->random(mt_rand(1, 5))->pluck('id');
        		$product->categories()->attach($categoryIds);
        	}
        );
        factory(Transaction::class, $transactionsCount)->create();
    }
}
