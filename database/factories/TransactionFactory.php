<?php

use Faker\Generator as Faker;
use App\Transaction;
use App\User;
use App\Seller;

$factory->define(Transaction::class, function (Faker $faker) {
	$seller = Seller::has('products')->newQuery()->inRandomOrder()->first();

    return [
        'quantity' => $faker->numberBetween(1, 3),
        'buyer_id' => function() use ($seller) {
        	return User::query()->where('id', '<>', $seller->id)->inRandomOrder()->first()->id;
        },
        'product_id' => function() use ($seller) {
        	return $seller->products()->inRandomOrder()->first()->id;
        }
    ];
});
